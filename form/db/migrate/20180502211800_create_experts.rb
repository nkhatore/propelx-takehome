class CreateExperts < ActiveRecord::Migration[5.1]
  def change
    create_table :experts do |t|
      t.string :name
      t.string :role
      t.string :linkedin
      t.string :researchgate
      t.text :bio

      t.timestamps
    end
  end
end
