# Propel(x) Take-home Assignment README #

I created an Expert model and controller, that supported fields for name, role,
bio, a picture, and LinkedIn and ResearchGate profiles. To support user variance
when filling out the form, I made Name and Role required input fields and the
rest permitted but not mandatory. I began work on a Relationships model to
implement a followers system.
