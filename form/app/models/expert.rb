class Expert < ApplicationRecord
  validates :name, presence: true
  validates :role, presence: true
  has_many :relationships
end
