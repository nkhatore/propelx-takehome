class ExpertsController < ApplicationController
  def index
    @experts = Expert.all
  end

  def show
    @expert = Expert.find(params[:id])
    @relationships = Relationship.where(follower_id: @expert.id)
  end

  def new
    @expert = Expert.new
  end

  #def edit
  #  @expert = Expert.find(params[:id])
  #end

  def create
    @expert = Expert.new(expert_params)
    #render plain: params[:expert].inspect
    if @expert.save
      redirect_to @expert
    else
      render 'new'
    end
  end

  private
    def expert_params
      params.require(:expert).permit(:name, :role, :linkedin, :researchgate, :bio)
    end
end
